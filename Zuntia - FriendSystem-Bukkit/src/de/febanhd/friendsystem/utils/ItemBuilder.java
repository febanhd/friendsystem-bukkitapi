package de.febanhd.friendsystem.utils;
import java.util.Arrays;

import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;


public class ItemBuilder {
	
	private ItemStack item;
	private ItemMeta itemMeta;
	
	public ItemBuilder(Material material) {
		item = new ItemStack(material);
		itemMeta = item.getItemMeta();
	}
	
	public ItemBuilder(ItemStack itemStack) {
		item = itemStack;
		itemMeta = item.getItemMeta();
	}
	
	public ItemBuilder setDisplayName(String name) {
		itemMeta.setDisplayName(name);
		return this;
	}
	
	public ItemBuilder setOwner(String owner) {
		try {
			SkullMeta meta = (SkullMeta) this.itemMeta;
			meta.setOwner(owner);
		} catch (Exception e) {
			System.out.println("Failed to set the SkullOwner in Feban's ItemBuilder.\n"  + e.getMessage()); 
		}
		return this;
	}
	
	public ItemBuilder setlore(String... lore) {
		itemMeta.setLore(Arrays.asList(lore));
		return this;
	}
	
	public ItemBuilder addEnchantment(Enchantment ench, int level) {
		itemMeta.addEnchant(ench, level, true);
		return this;
	}
	
	public ItemBuilder setUnbreakeble() {
		itemMeta.spigot().setUnbreakable(true);
		return this;
	}
	
	public ItemBuilder setAmount(int amount) {
		item.setAmount(amount);
		return this;
	}
	
	public ItemBuilder hideEnchantments() {
		itemMeta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
		return this;
	}
	
	public ItemBuilder addFlag(ItemFlag flag) {
		itemMeta.addItemFlags(flag);
		return this;
	}
	
	public ItemStack build() {
		item.setItemMeta(itemMeta);
		return item;
	}
	
	
	@SuppressWarnings("deprecation")
	public ItemBuilder createItem(int itemid, int amount, short damage, byte subid) {
		item = new ItemStack(itemid, amount, damage, subid);
	return this;
	}

}
