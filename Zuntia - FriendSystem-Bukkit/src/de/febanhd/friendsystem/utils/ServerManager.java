package de.febanhd.friendsystem.utils;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;

import org.bukkit.entity.Player;

import de.febanhd.friendsystem.FriendSystem;

public class ServerManager {
	
	public static void sendPlayerToServer(Player player, String server){    	 
        ByteArrayOutputStream b = new ByteArrayOutputStream();
        DataOutputStream out= new DataOutputStream(b);
        try{
            out.writeUTF("Connect"); 
            out.writeUTF(server);
        } catch (Exception e) {
            e.printStackTrace();
            player.sendMessage(FriendSystem.PREFIX + "�cEs gab einen Fehler. Bitte melde doch bei einem Teammitglied!");
        }
        player.sendPluginMessage(FriendSystem.instance, "BungeeCord", b.toByteArray());
    }

}
