package de.febanhd.friendsystem.listener;

import java.sql.SQLException;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType.SlotType;

import de.febanhd.friendsystem.FriendSystem;
import de.febanhd.friendsystem.api.FriendManager;
import de.febanhd.friendsystem.gui.FriendMenuGUI;
import de.febanhd.friendsystem.gui.PersonalFriendGUI;
import de.febanhd.friendsystem.utils.NameFetcher;
import net.md_5.bungee.api.ChatColor;

public class GUIListener implements Listener {
	
	public GUIListener() {
		Bukkit.getPluginManager().registerEvents(this, FriendSystem.instance);
	}
	
	@EventHandler
	public void onInventoryClick(InventoryClickEvent event) throws SQLException {
		if(event.getCurrentItem() == null) return;
		if(event.getSlotType() == SlotType.OUTSIDE) return;
		if(event.getCurrentItem() == null) return;
		FriendManager manager = FriendSystem.instance.friendManager;
		String title = event.getInventory().getTitle();
		title = ChatColor.stripColor(title.toLowerCase());
		Player player = (Player) event.getWhoClicked();
		if(title.startsWith(ChatColor.stripColor(FriendMenuGUI.GUI_NAME.toLowerCase()))) {
			event.setCancelled(true);
			if(event.getCurrentItem().getType().equals(Material.BARRIER)) return;
			switch(event.getSlot()) {
			case 53:
				String[] str = title.split("seite ");
				int page = Integer.valueOf(str[1]);
				if(page < 5)
					new FriendMenuGUI(player, FriendSystem.instance.friendManager.getFriends(player.getUniqueId())).open(page);
				break;
			case 52:
				String[] str2 = title.split("seite ");
				int page2 = Integer.valueOf(str2[1]);
				if((page2-1) > 0)
					new FriendMenuGUI(player, FriendSystem.instance.friendManager.getFriends(player.getUniqueId())).open(page2-2);
				break;
				
			default:
				if(!event.getCurrentItem().hasItemMeta()) return;
				String friend = ChatColor.stripColor(event.getCurrentItem().getItemMeta().getDisplayName());
				new PersonalFriendGUI(player, friend).open();
				break;
			}
		}
		if(title.startsWith(ChatColor.stripColor(PersonalFriendGUI.GUI_NAME.toLowerCase()))) {
			event.setCancelled(true);
			String friend = ChatColor.stripColor(event.getInventory().getItem(10).getItemMeta().getDisplayName());
			switch(event.getSlot()) {
			case 12:
				player.closeInventory();
				manager.jump(player, friend);
				break;
			case 14:
				player.closeInventory();
				//TODO: Invite friend to party. Execute /party invite friend command on ProxyServer
				break;
			case 16:
				player.closeInventory();
				FriendSystem.instance.friendManager.removeFriend(player.getUniqueId(), UUID.fromString(NameFetcher.getUUID(friend)));
				break;
				
			default:
				break;
			}
		}
	}

}
