package de.febanhd.friendsystem.api;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import cloud.timo.TimoCloud.api.TimoCloudAPI;
import cloud.timo.TimoCloud.api.objects.PlayerObject;
import de.febanhd.friendsystem.FriendSystem;
import de.febanhd.friendsystem.utils.NameFetcher;
import de.febanhd.friendsystem.utils.ServerManager;
import net.Lenni0451.EasySQL.Database;
import net.Lenni0451.EasySQL.Row;
import net.Lenni0451.EasySQL.Table;
import net.Lenni0451.EasySQL.info.rows.RowInfo;

public class FriendManager {

	Database database;

	public FriendManager() {
		database = FriendSystem.instance.database;
	}

	public void openInventory(Player player) {

	}

	public List<String> getFriends(UUID uuid) {
		List<String> friends = new ArrayList<>();
		try {
			Table friendTable = database.getTable("friend");
			List<Row> rows = friendTable.getRowsWhere(new RowInfo("UUID", uuid.toString()));
			Row row = rows.get(0);
			String friendUUIDS = (String) row.getColumn("FRIENDS");
			if (friendUUIDS == null || friendUUIDS == "")
				return null;
			String[] args = friendUUIDS.split(";");
			for (int i = 0; i < args.length; i++) {
				
				String name = NameFetcher.getName(args[i]);
				if(!(name == null || name.equalsIgnoreCase("null") || name.contentEquals("")))
					friends.add(name);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return friends;
	}

	public List<String> getRequests(UUID uuid) {
		List<String> friends = new ArrayList<>();
		try {
			Table friendTable = database.getTable("friend");
			List<Row> rows = friendTable.getRowsWhere(new RowInfo("UUID", uuid.toString()));
			Row row = rows.get(0);
			String friendUUIDS = (String) row.getColumn("REQUESTS");
			if (friendUUIDS == null || friendUUIDS == "")
				return null;
			String[] args = friendUUIDS.split(";");
			for (int i = 0; i < args.length; i++) {
				friends.add(NameFetcher.getName(args[i]));
			}
			return friends;
		} catch (SQLException e) {
			return null;
		}
	}

	public void removeFriend(UUID uuid, UUID targetUUID) throws SQLException {
		Table friendTable = database.getTable("friend");
		// Remove targetsUUID of playersFriends
		String friends = (String) friendTable.getRowsWhere("UUID", uuid.toString()).get(0).getColumn("FRIENDS");
		friends = friends.replaceAll(targetUUID.toString() + ";", "");
		friendTable.getRowsWhere("UUID", uuid.toString()).get(0).updateColumn("FRIENDS", friends);
		// Remove playersUUID of tragetFriends
		String friendsTarget = (String) friendTable.getRowsWhere("UUID", targetUUID.toString()).get(0).getColumn("FRIENDS");
		friendsTarget = friendsTarget.replaceAll(uuid.toString() + ";", "");
		friendTable.getRowsWhere("UUID", targetUUID.toString()).get(0).updateColumn("FRIENDS", friendsTarget);
		Bukkit.getPlayer(uuid).sendMessage(FriendSystem.PREFIX + "�7Du bist nun nicht mehr mit �e" + Bukkit.getOfflinePlayer(targetUUID).getName() + " �7befreundet.");
	}
	
	public void jump(Player player, String targetName) {
		if(TimoCloudAPI.getUniversalAPI().getPlayer(targetName).isOnline()) {
			String server = getServer(targetName);
			if(server != null)
				ServerManager.sendPlayerToServer(player, server);
		}
	}

	public String getServer(String string) {
		try {
			PlayerObject target = TimoCloudAPI.getUniversalAPI().getPlayer(string);
			if(target.isOnline())
			return target.getServer().getName();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "�cLade...";
	}

}
