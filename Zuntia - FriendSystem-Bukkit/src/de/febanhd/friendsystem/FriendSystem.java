package de.febanhd.friendsystem;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import de.febanhd.friendsystem.api.FriendManager;
import de.febanhd.friendsystem.listener.GUIListener;
import net.Lenni0451.EasySQL.Database;
import net.Lenni0451.EasySQL.Table;
import net.Lenni0451.EasySQL.info.column.impl.BasicColumnInfo;
import net.Lenni0451.EasySQL.info.column.impl.defaults.MySQLDatabase;
import net.md_5.bungee.api.chat.TextComponent;

public class FriendSystem extends JavaPlugin {
	
	public Database database;

	public static String db;
	public static String port;
	public static String user;
	public static String pw;
	public static String host;
	
	public static final String PREFIX = "�7[�eFreunde�7] �r";
	public static FriendSystem instance;
	
	public FriendManager friendManager;

	static {
		db = "friendsystem";
		port = "3306";
		user = "root";
		pw = "6aa5a43b3778";
		host = "127.0.0.1";
	}
	
	@Override
	public void onEnable() {
		instance = this;
		getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
		if(connect()) {
			friendManager = new FriendManager();
			new GUIListener();
		}
	}
	
	@Override
	public void onDisable() {
		try {
			database.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public boolean connect() {
		Bukkit.getConsoleSender().sendMessage(PREFIX + "�eVersucht eine MySQL verbindung herzustellen.");
		try {
			final String connectionString = "jdbc:mysql://" + host + ":" + port + "/" + db + "?useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
			Connection connection = DriverManager.getConnection(connectionString, user, pw);
			Database database = new Database(connection);
			Bukkit.getConsoleSender().sendMessage(PREFIX + "�aVerbindung erfolgreich aufgebaut.");
			this.database = database;
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			Bukkit.getConsoleSender().sendMessage(PREFIX + "�cVerbindug konnte nicht hergestellt werden!");
			return false;
		}
	}
}
