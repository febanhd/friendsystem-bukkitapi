package de.febanhd.friendsystem.gui;

import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

import de.febanhd.friendsystem.FriendSystem;
import de.febanhd.friendsystem.utils.ItemBuilder;

public class PersonalFriendGUI {
	
	private Player player;
	private String friendName;
	private Inventory inventory;
	public static final String GUI_NAME = "�eFreund ";
	
	public PersonalFriendGUI(Player player, String friend) {
		this.player = player;
		this.friendName = friend;
		inventory = Bukkit.createInventory(player, 9*3, GUI_NAME + "�c" + friendName);
	}
	
	public void open() {
		for(int i = 0; i < 27; i++) {
			switch (i) {
			case 0: case 1: case 2: case 3: case 4: case 5: case 6: case 7: case 8: case 9: case 17: case 18: case 19: case 20: case 21: case 22: case 23: case 24: case 25: case 26: case 27:
				inventory.setItem(i, new ItemBuilder(Material.STAINED_GLASS_PANE).createItem(160, 1, (byte) 1, (byte)7).setDisplayName("�b").build());
				break;
			case 10:
				inventory.setItem(10, getOnlineItem(friendName));
				break;
			case 12:
				inventory.setItem(i, new ItemBuilder(Material.EYE_OF_ENDER).setDisplayName("�5Hinterherspringen").build());
				break;
			case 14:
				inventory.setItem(i, new ItemBuilder(Material.CAKE).setDisplayName("�eIn eine Party einladen.").build());
				break;
			case 16:
				inventory.setItem(i, new ItemBuilder(Material.BARRIER).setDisplayName("�cFreund Entfernen").build());
				break;
			}
		}
		player.openInventory(inventory);
	}
	
	private ItemStack getOnlineItem(String playerName) {
		ItemStack item = new ItemStack(Material.SKULL_ITEM, 1, (byte) 3);
		SkullMeta meta = (SkullMeta) item.getItemMeta();
		meta.setDisplayName("�7" + playerName);
		meta.setLore(Arrays.asList("�8Onlinestatus: �aOnline", "�7Server: �b"));
		meta.setOwner(playerName);
		item.setItemMeta(meta);
		return item;
	}

}
