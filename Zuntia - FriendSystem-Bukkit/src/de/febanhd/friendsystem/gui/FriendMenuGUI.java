package de.febanhd.friendsystem.gui;

import java.awt.RadialGradientPaint;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import cloud.timo.TimoCloud.api.TimoCloudAPI;
import cloud.timo.TimoCloud.api.objects.PlayerObject;
import de.febanhd.friendsystem.FriendSystem;
import de.febanhd.friendsystem.api.FriendManager;
import de.febanhd.friendsystem.utils.ItemBuilder;
import de.febanhd.friendsystem.utils.ItemSkulls;
import de.febanhd.friendsystem.utils.NameFetcher;
import net.Lenni0451.EasySQL.Table;

public class FriendMenuGUI {

	private Player player;
	private List<String> friends;
	private Inventory inventory;
	public static String GUI_NAME = "�eFreundesmen�";

	public FriendMenuGUI(Player player, List<String> friends) {
		this.player = player;
		this.friends = friends;
	}
	
	
	//@Developer: If you want to open the normal GUI (For a LobbySystem etc.) you use as page "0"
	public void open(int page) {
		FriendManager manager = FriendSystem.instance.friendManager;
		inventory = Bukkit.createInventory(player, 9 * 6, GUI_NAME + ": �7Seite " + (page + 1));
		List<String> online = new ArrayList<String>();
		List<String> offline = new ArrayList<String>();
		List<String> friends = new ArrayList<String>();
		if (this.friends != null) {
			for (String str : this.friends) {
				if (isOnline(NameFetcher.getUUID(str)))
					online.add(str);
				else
					offline.add(str);
			}
		}
		for (String str : online)
			friends.add(str);
		for (String str : offline)
			friends.add(str);
		if(friends.size() == 0)
			inventory.setItem(22, new ItemBuilder(Material.BARRIER).setDisplayName("�cEs scheint als h�ttest du noch keine Freunde!").setlore("�7F�ge welche mit �e/friend add <Spieler> �7hinzu.").build());
		switch (page) {
		case 0:
			inventory.setItem(53, new ItemBuilder(getSkullByURL("682ad1b9cb4dd21259c0d75aa315ff389c3cef752be3949338164bac84a96e")).setDisplayName("�7N�chste Seite").build());
			for (int i = (0 * (page + 1)); i < friends.size() && i < 45 && i < 200; i++) {
				String name = friends.get(i);
				if (online.contains(name))
					inventory.setItem(i, getOnlineItem(name));
				else
					inventory.setItem(i, getOfflineItem(name));
			}
			for(int i = 45; i < 53; i++)
				inventory.setItem(i, new ItemBuilder(Material.STAINED_GLASS_PANE).createItem(160, 1, (byte) 1, (byte)7).setDisplayName("�b").build());
			break;
		default:
			inventory.setItem(52, new ItemBuilder(getSkullByURL("37aee9a75bf0df7897183015cca0b2a7d755c63388ff01752d5f4419fc645")).setDisplayName("�7Vorherige Seite").build());
			inventory.setItem(53, new ItemBuilder(getSkullByURL("682ad1b9cb4dd21259c0d75aa315ff389c3cef752be3949338164bac84a96e")).setDisplayName("�7N�chste Seite").build());
			for (int i = 0; i < friends.size() && i < 45 && i < 200; i++) {
				if (friends.size() > i + (45 * (page + 1))) {
					String name = friends.get(i + (45 * (page + 1)));
					if (online.contains(name))
						inventory.setItem(i, getOnlineItem(name));
					else
						inventory.setItem(i, getOfflineItem(name));
				}
			}
			for(int i = 45; i < 52; i++)
				inventory.setItem(i, new ItemBuilder(Material.STAINED_GLASS_PANE).createItem(160, 1, (byte) 1, (byte)7).setDisplayName("�b").build());
			break;
		}
		player.openInventory(inventory);
	}

	private boolean isOnline(String uuid) {
		try {
			PlayerObject player = TimoCloudAPI.getUniversalAPI().getPlayer(NameFetcher.getName(uuid));
			if(player == null) return false;
			return player.isOnline();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	@SuppressWarnings("deprecation")
	private ItemStack getOfflineItem(String name) {
		ItemStack item = new ItemStack(397, 1, (short) 1, (byte) 0);
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName("�7" + name);
		meta.setLore(Arrays.asList("�8Onlinestatus: �cOffline"));
		item.setItemMeta(meta);
		return item;
	}

	private ItemStack getOnlineItem(String playerName) {
		ItemStack item = new ItemStack(Material.SKULL_ITEM, 1, (byte) 3);
		SkullMeta meta = (SkullMeta) item.getItemMeta();
		meta.setDisplayName("�7" + playerName);
		meta.setLore(Arrays.asList("�8Onlinestatus: �aOnline", "�7Server: �b" + FriendSystem.instance.friendManager.getServer(playerName)));
		meta.setOwner(playerName);
		item.setItemMeta(meta);
		return item;
	}
	
	private ItemStack getSkullByURL(String skinUrl) {
		ItemStack item = ItemSkulls.getSkull("http://textures.minecraft.net/texture/" + skinUrl);
		return item;
	}

}
